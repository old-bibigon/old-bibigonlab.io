# -*- coding: utf-8 -*-

# Copyright © 2017-2021 Roberto Alsina and others.

# Permission is hereby granted, free of charge, to any
# person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the
# Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the
# Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice
# shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""Listing shortcode (equivalent to reST’s listing directive)."""

import os

import pygments

from nikola.plugin_categories import ShortcodePlugin
from nikola import utils


class Plugin(ShortcodePlugin):
    """Plugin for listing shortcode."""

    name = "listing_rel"

    def handler(self, fname, language=None, linenumbers=False, copy_source=True, filename=None, site=None, data=None, lang=None, post=None):
        """Create HTML for a listing."""
        fname = fname.replace('/', os.sep)
        source_dir = os.path.dirname(post.source_path)
        fpath = os.path.join(source_dir, fname)

        #для проверки что из каталога не вылазят
        nikola_dir = os.path.dirname(os.path.abspath(site.configuration_filename))
        fpath_abs_path = os.path.abspath(fpath)

        if not os.path.isfile(fpath) or nikola_dir not in fpath_abs_path:
            output = '!not found file {0}'.format(fpath)
            return output, []

        linenumbers = 'table' if linenumbers else False
        deps = [fpath]

        src_link = ''
        if isinstance(copy_source, str) and copy_source.lower() in ('false', 'f', 'no', '0'):
            copy_source = False

        if copy_source:
            output_name = os.path.join(
                            site.config["OUTPUT_FOLDER"],
                            post.folder,
                            fname
                            )
            src_label = self.site.MESSAGES('Source')
            src_link = '<a href="{0}">({1})</a>'.format(fname, src_label)
            utils.copy_file(fpath, output_name)

        with open(fpath, 'r') as inf:
            data = inf.read()
            if language is None:
                lexer = pygments.lexers.get_lexer_for_filename(os.path.basename(fname))
            else:
                lexer = pygments.lexers.get_lexer_by_name(language)
            formatter = pygments.formatters.get_formatter_by_name(
                'html', linenos=linenumbers)
            src_target = fname
            output = '{0} {1}:'.format(
                fname, src_link) + pygments.highlight(data, lexer, formatter)

        return output, deps

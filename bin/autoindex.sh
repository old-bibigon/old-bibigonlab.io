#!/bin/bash
# создаём index.md из файлов текущих

INDEX_FILE="index.md"

[ -f "${INDEX_FILE}" ] || cat >"${INDEX_FILE}" <<EOF
title: 
date: `date '+%F %T UTC%:z'`
tags: 
category: 
link: 
description: 
type: text


EOF

function index_add(){
    grep "(${1})" "${INDEX_FILE}" && return
    echo "- [${1}](${1})" >> "${INDEX_FILE}"
}

for f in *.{md,rst,ipynb,html};do
    [ "${f%%.*}" == "index" ] && continue
    [ -f "${f}" ] && index_add "${f%%.*}"
done

for f in *;do
    [ -d "${f}" ] && index_add "${f}"
done

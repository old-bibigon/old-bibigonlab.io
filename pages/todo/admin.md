title: Админские TODO
date: 2021-04-14 02:06:54 UTC+03:00
tags: todo, admin
category: 
link: 
description: 
type: text


# ansible #
  * надёргать примеры для основных модулей в example


# k8s #
Описать основные yml в example:

- [ ] deployment



# поиграться с #
- поиграть с aqua и clair
- <https://github.com/coreos/clair>
- [microscanner](http://github.com/aquasecurity/microscanner)
- [kubeval](http://github.com/instrumenta/kubeval)
- [helm-monitor](https://github.com/ContainerSolutions/helm-monitor)
- [kompose](http://github.com/kubernetes/kompose)
- [prometeus](https://habr.com/ru/company/southbridge/blog/455290/)
- <https://github.com/korfuri/django-prometheus>
- [kustomize](https://habr.com/ru/company/flant/blog/469179/)

- haproxy

# что за хрень #
- [ ] Уверенное ориентирование в техстеке linux/postgres (11, 12)/minio/keycloak
- [ ] Graylog, FluentD, Elasticsearch, Solr, Jaeger
- [ ] The Twelve-Factor App для вас не просто набор слов;
- [ ] Опыт администрирования систем управления репозиториями: Nexus, Artifactory, Registry;

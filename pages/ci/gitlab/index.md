title: GitLab
date: 2021-04-09 16:20:11 UTC+03:00
tags: 
category: 
link: 
description: 
type: text


# Переменные #

[Встроенные](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

[Настройки](https://docs.gitlab.com/ee/ci/yaml/README.html)

[Синтаксис only](https://docs.gitlab.com/ee/ci/yaml/README.html#onlyexcept-basic)


# Настройки #

Удаление репозитария
`General > Advanced`

Видимость репозитария
`General > Visibility`

Очистка образов и артефактов
`CI/CD >  Clean up image tags`


# Логи запуска #
Тегированный релиз:

{{% listing_rel job-tag.log bash %}}


Master branch:

{{% listing_rel job-master.log bash %}}


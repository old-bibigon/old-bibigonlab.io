title: плагины nikola
date: 2021-04-19 23:51:41 UTC+03:00
tags: 
category: 
link: 
description: 
type: text


# Listing_rel #
Костыльнул listing, т.к. тот хочет брать только из каталога listing

 * `fname` - путь к файлу
 * `language='None'` - язык разметки, [список](https://pygments.org/languages/);
   None - угадывает по имени файла
 * `linenumbers=False` - показывать номера строк
 * `copy_source=True` - копировать исходный файл в output

----
Проверка

{{% listing_rel plugins.md %}}

{{% listing_rel ../index.md %}}

{{% listing_rel /etc/passwd YAML %}}

{{% listing_rel "../../../../../../../../etc/passwd" YAML+Jinja %}}

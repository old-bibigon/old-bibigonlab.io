title: Test Mindmap Plantuml
date: 2021-04-09 16:20:11 UTC+03:00
tags: mindmap, plantuml
category: 
link: 
description: 
type: text

# Описание
Куча разных типов диаграмм, [link](https://plantuml.com/ru/)


# Установка
В деб10 старый, не умеет mindmap, тащить jar с сайта
Возможно нужен graphviz, но он типа в jar должен быть


# Тестовый

```plantuml
@startuml
Alice -> Bob: test
@enduml
```

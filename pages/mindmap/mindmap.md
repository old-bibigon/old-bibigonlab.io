title: Test Mindmap Plantuml
date: 2021-04-09 16:20:11 UTC+03:00
tags: mindmap, plays
category: mindmap
link: 
description: 
type: text

```plantuml
@startmindmap
* OS
** Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** LMDE
** SolydXK
** SteamOS
** Raspbian

left side

** Windows 95
** Windows 98
** Windows NT
*** Window XP
*** Windows 8
***[#lightgreen] Windows 10
***: Example 1
multiline
string
;

@endmindmap
```
